create table clients(
    id int(11) primary key AUTO_INCREMENT,
    full_name varchar(128) not null,
    abbreviation varchar(6) not null
);