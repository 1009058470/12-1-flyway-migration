create table contract(
    id int(11) primary key AUTO_INCREMENT,
    name varchar(128) not null,
    client_id int not null,
    foreign key (client_id) references clients(id)
);