create table services(
    id int(11) primary key AUTO_INCREMENT,
    branch_id int(11),
    type varchar(128) not null,
    description varchar(10000) not null,
    foreign key(branch_id) references clients(id)
);