-- 在这必须要加sql的后缀，不然他不会被管理

create table offices(
    id int(11) primary key AUTO_INCREMENT,
    country varchar(100) not null,
    city varchar(100) not null
);

create table staff(
    id int(11) primary key AUTO_INCREMENT,
    office_id int(11),
    first_name varchar(100) not null,
    last_name varchar(100) not null,
    foreign key(office_id) references offices(id)
);