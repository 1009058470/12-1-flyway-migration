create table contract_services_offices(
    id int(11) primary key AUTO_INCREMENT,
    office_id int(11),
    contract_id int(11),
    foreign key (office_id) references offices(id),
    foreign key (contract_id) references contract(id)
);